#------------------------------------------------------------
# pyrics/Representations/ConvolutionalDSF.py
#
# Representation of a DSF in convolutional form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from .SystemRepresentation import SystemRepresentation
from .ConvolutionalTF import ConvolutionalTF


class ConvolutionalDSF(SystemRepresentation):
  '''Representation of a DSF where each entry is in convolutional form.

  Models

      Y(t) = Q(t) * Y(t) + P(t) * U(t),

  where * is the convolution operation and each Q_ij(t), P_ij(t) is an
  impulse response in convolutional form; i.e.,

      Q_ij(t) = a delta(t, 0) + sum_{n = 1}^w b_n c_n^t

  for parameters a, b_n, and c_n, n = 1, ..., w.

  Parameters
  ----------
  Q : numpy array (p x p)
  P : numpy array (p x m)
  '''

  #------------------------------------------------------------
  def __init__(self, Q, P):
    self.Q = Q
    self.P = P

    if not isinstance(Q, ConvolutionalTF):
      raise ValueError('Q must be a ConvolutionalTF; given {}'.format(type(Q)))
    if not isinstance(P, ConvolutionalTF):
      raise ValueError('P must be a ConvolutionalTF; given {}'.format(type(P)))

    assert len(Q.shape) == 2
    assert len(P.shape) == 2

    p, p1 = Q.shape
    p2, m = P.shape

    if p != p1:
      raise ValueError(
        'Dimension Mismatch: Q must be square. Got dimensions ({}, {})'.format(
          p, p1
        )
      )

    if p != p2:
      raise ValueError(
        'Dimension Mismatch: Q and P must have the same number of '
        'rows. Got dimensions ({}, {}) and ({}, {})'.format(
            p, p1, p2, m
        )
      )

    self.p = p
    self.m = m

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  @property
  def shape(self):
    '''Determine the state of this DSF.

    Given the dimensions of Q as p x p and the dimensions of P as p x m, the
    shape is (p, m).
    '''
    return self.p, self.m

  #####################################################################
  #   Conversion functions
  #####################################################################

  #------------------------------------------------------------
  def to_DSF(self):
    '''Convert this ConvolutionalDSF to a DiscreteDSF/

    Returns
    -------
    F : DiscreteDSF
    '''
    from .DiscreteDSF import DTDSF

    Q = self.Q.to_TF()
    P = self.P.to_TF()

    return DTDSF(Q, P)

  #------------------------------------------------------------
  def to_impulse(self, r):
    '''Convert this ConvolutionalDSF into an ImpulseDSF.

    Returns
    -------
    F : ImpulseDSF (p x m)
    '''
    from .ImpulseDSF import ImpulseDSF

    Qimpulse = self.Q.to_impulse(r)
    Pimpulse = self.P.to_impulse(r)

    return ImpulseDSF(Qimpulse, Pimpulse)


  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    # return repr(self.G)
    return 'Q = {}\nP = {}'.format(repr(self.Q), repr(self.P))

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Return the latex formatted version of this DSF.

    Parameters
    ----------
    Qname : str (latex formatting)
        The name to give to the Q matrix.
    Pname : str (latex formatting)
        The name to give to the P matrix.

    Returns
    -------
    latex : str
    '''
    Qname = kwargs.get('Qname', 'Q')
    Pname = kwargs.get('Pname', 'P')
    return r'{} \qquad {}'.format(
      self.Q.latex(name=Qname), self.P.latex(name=Pname)
    )
