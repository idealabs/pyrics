#------------------------------------------------------------
# pyrics/Plot/PlotSimulation.py
#
# Plotting utilities to show input-output data.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

import plotly.graph_objs as go
from plotly import tools


def plotSimulation(Du, Dy, plot):
  '''Create a plot of the inputs in Du and the outputs in Dy.

  Only one plot will be generated, but with two subfigures. Inputs will all be
  drawn on the top subfigure, and outputs will all be drawn on the bottom
  subfigure.

  Parameters
  ----------
  Du : numpy array (T x m)
    The input data, where the t'th row of Du is u(t) (transpose).
  Du : numpy array (T x p)
    The output data, where the t'th row of Dy is y(t) (transpose).
  plot : function (plotly plot function)
    Plots a single plot of the input/output data using that function.
  '''
  T, p = Du.shape
  T1, m = Dy.shape

  if T1 != T:
    raise ValueError((
      'Du and Dy must be the same length (same number of rows). Given '
      'Du with {} rows and Dy with {} rows.'
    ).format(T, T1))

  fig = tools.make_subplots(
    rows=2, cols=1, print_grid=False,
    subplot_titles=['Input Data', 'Output Data']
  )

  for i in range(m):
    trace = go.Scatter(
      x=list(range(T)),
      y=Du[:, i],
      name=r'$u_{}(t)$'.format(i + 1)
    )
    fig.append_trace(trace, 1, 1)

  for i in range(p):
    trace = go.Scatter(
      x=list(range(T)),
      y=Dy[:, i],
      name=r'$y_{}(t)$'.format(i + 1)
    )
    fig.append_trace(trace, 2, 1)

  plot(fig)
