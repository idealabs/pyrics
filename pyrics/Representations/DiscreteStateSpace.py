#------------------------------------------------------------
# pyrics/Representations/DiscreteStateSpace.py
#
# Representation of a discrete-time state space model.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from operator import gt, ge
import numpy as np
from sympy import latex
from sympy.matrices import Matrix, eye
from sympy.abc import z

from .DiscreteTime import DiscreteTime


class DTSS(DiscreteTime):
  '''A discrete time state space model, where dynamics are given by:

    x[k + 1] = Ax[k] + Bu[k]
    y[k] = Cx[k] + Du[k]

  where k is a time index, x[k] is in R^n, u[k] is in R^m, and y[k]
  is in R^p.

  Parameters
  ----------
  A : numpy.array (n x n)
  B : numpy.array (n x m)
  C : numpy.array (p x n)
  D : numpy.array (p x m) or None, default=None
    If not given, will be initialized to an array of zeros.
  '''

  #------------------------------------------------------------
  def __init__(self, A, B, C, D=None):
    A = np.array(A)
    B = np.array(B)
    C = np.array(C)

    # Compute Dimensions
    n, n1 = A.shape
    n2, m = B.shape
    p, n3 = C.shape

    # Initialize D if needed
    if D is None:
      D = np.zeros((p, m))
    else:
      D = np.array(D)

    p1, m1 = D.shape

    # Check Dimensions
    if n1 != n:
      raise ValueError(
        'A must be square. Given A as ({} x {})'.format(n, n1)
      )
    if n2 != n:
      raise ValueError((
        'B must have the same number of rows as A. Given A as ' # pylint: disable=W1308
        '({} x {}) but B as ({} x {})'
      ).format(n, n, n2, m))
    if n3 != n:
      raise ValueError((
        'C must have the same number of columns as A. Given A as ' # pylint: disable=W1308
        '({} x {}) but C as ({} x {})'
      ).format(n, n, p, n3))
    if p1 != p:
      raise ValueError((
        'D must have the same number of rows as C. Given C as '
        '({} x {}) but D as ({} x {})'
      ).format(p, n3, p1, m1))
    if m1 != m:
      raise ValueError((
        'D must have the same number of columns as B. Given B as '
        '({} x {}) but D as ({} x {})'
      ).format(n2, m, p1, m1))

    # Register Matrices and Dimensions
    self.A = A
    self.B = B
    self.C = C
    self.D = D

    self.n = n
    self.m = m
    self.p = p

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  def simulate(self, Du, x0=None):
    '''Simulate the system with the inputs in Du.

    Parameters
    ----------
    Du : numpy.array (T x m)
      The inputs where each row k is u(k)^T.
    x0 : numpy.array (n x 1)
      The initial conditions. NOT YET IMPLEMENTED.

    Returns
    -------
    Dy : numpy.array (T x p)
      The simulated outputs given the inputs and initial conditions, where
      each row k is y(k)^T.
    '''
    assert x0 is None  # TODO

    A = self.A
    B = self.B
    C = self.C
    D = self.D

    if x0 is None:
      x0 = np.zeros((self.n, 1))

    T, m = Du.shape
    assert m == self.m
    Dy = np.zeros((T, self.p))

    x = x0
    x = x.reshape((self.n, 1))
    for i in range(T):
      u = Du[i, :]
      u = u.reshape((self.m, 1))
      y = C.dot(x) + D.dot(u)
      y = y.reshape((self.p, 1))
      x = A.dot(x) + B.dot(u)
      x = x.reshape((self.n, 1))
      Dy[i, :] = y.T

    return Dy

  #------------------------------------------------------------
  def is_stable(self, include_marginally=False):
    '''Determine whether this system is stable.

    The conditions of stability are as follows:
      - If the magnitude of *all* eigenvalues of A are strictly less than
        one, the system is (asymtotically) stable.
      - If the magnitude of *all* eigenvalues of A are less than or equal
        to one, and at least one eigen value has magnitude equal to one,
        then the system is marginally stable.
      - If the magnitude of *any* eigenvalue is strictly greater than
        one, then the system is unstable.

    Examples
    --------
    Check to see if the DTSS system `ss` is asymptotically stable:

      ss.is_stable() == True

    Check to see if the DTSS system `ss` is either asymptotically stable
    or marginally stable:

      ss.is_stable(include_marginally=True) == True

    Check to see if the DTSS system `ss` is marginally stable only:

      (ss.is_stable(True) and not ss.is_stable()) == True

    Parameters
    ----------
    include_marginally : bool, default=True
      If True, marginally stable systems are considered to be stable,
      otherwise, marginally stable systems are considered to be unstable.

    Returns
    -------
    is_stable : bool
    '''
    eigs, _ = np.linalg.eig(self.A)

    if include_marginally:
      comp = gt
    else:
      comp = ge

    for eig in eigs:
      if comp(abs(eig), 1):
        return False
    return True

  #####################################################################
  #   Conversions
  #####################################################################

  #------------------------------------------------------------
  def to_TF(self):
    '''Converts this state space model into a transfer function matrix.

    Returns
    -------
    G : DTTF
      The discrete time transfer function matrix representation of this system.
    '''
    from . import DTTF

    Asym = Matrix(self.A)
    Bsym = Matrix(self.B)
    Csym = Matrix(self.C)
    Dsym = Matrix(self.D)
    Isym = eye(self.n)
    G = Csym * (z * Isym - Asym).inv() * Bsym + Dsym

    return DTTF(G)

  #------------------------------------------------------------
  def to_DSF(self, factor=True):
    '''Convert this state space model into a DSF.

    TODO : Generalize
      FOR NOW ASSUMES THAT C = [I 0]
    TODO : take advantage of factor

    Parameters
    ----------
    factor : bool, default=True
      If True, simplifies and factors all rational polynomials (coefficients
      should be rational). Otherwise only simplifies.

    Returns
    -------
    QP : DTDSF
        The discrete time DSF representation of this system.
    '''
    from . import DTTF, DTDSF

    A = self.A
    B = self.B
    C = self.C
    D = self.D

    p, _ = C.shape
    n, _ = A.shape
    # h, m = B.shape

    A11 = Matrix(A[:p, :p])
    if p < n:
      A12 = Matrix(A[:p, p:n])
      A21 = Matrix(A[p:n, :p])
      A22 = Matrix(A[p:n, p:n])
    else:
      A12 = Matrix([])
      A21 = Matrix([])
      A22 = Matrix([])

    B1 = B[:p, :]
    B2 = B[p:, :]

    # l, _ = A22.shape
    l = n - p
    Isym = eye(l)

    if p != n:
      W = A11 + A12 * (z * Isym - A22).inv() * A21
      V = B1 + A12 * (z * Isym - A22).inv() * B2
    else:
      W = A11
      V = B1

    Wdiag = W * 0
    for i in range(p):
      Wdiag[i, i] = W[i, i]

    I2 = eye(p)
    Wdinv = (z * I2 - Wdiag).inv()
    Q = Wdinv * (W - Wdiag)
    P = Wdinv * V + (I2 - Q) * Matrix(D)


    if factor:
      pass  # TODO use or remove

    return DTDSF(DTTF(Q), DTTF(P))

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    # return repr(self.G)
    return r'A = {}\nB = {}\nC={}\nD={}'.format(
      repr(self.A), repr(self.B), repr(self.C), repr(self.D)
    )

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this state space model.

    kwargs
    ------
    Mname : str
      The name to give to matrix `M`, where M is any of A, B, C, or D.

    Returns
    -------
    latex : str
    '''
    Aname = kwargs.get('Aname', 'A')
    Bname = kwargs.get('Bname', 'B')
    Cname = kwargs.get('Cname', 'C')
    Dname = kwargs.get('Dname', 'D')

    return (
      r'{} = {} \qquad {} = {}\\\\{} = {} \qquad {} = {}'
    ).format(
      Aname, latex(Matrix(self.A)), Bname, latex(Matrix(self.B)),
      Cname, latex(Matrix(self.C)), Dname, latex(Matrix(self.D))
    )
