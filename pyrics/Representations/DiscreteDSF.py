#------------------------------------------------------------
# pyrics/Representations/DiscreteDSF.py
#
# Representation of a discrete-time DSF.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

# import warnings
from sympy import oo
from sympy.matrices import Matrix, eye
from numpy.linalg import matrix_rank as rank

from .DiscreteTime import DiscreteTime


class DTDSF(DiscreteTime):
  '''Representation of a discrete time dynamical structure function.

  Models

    Y(z) = Q(z)Y(z) + P(z)U(z)

  Parameters
  ----------
  Q : DTTF
  P : DTTF
  '''

  #------------------------------------------------------------
  def __init__(self, Q, P):
    from . import DTTF

    if not isinstance(Q, DTTF):
      raise ValueError('Q must be a DTTF')
    if not isinstance(P, DTTF):
      raise ValueError('P must be a DTTF')

    p, p1 = Q.shape
    p2, m = P.shape

    if p != p1:
      raise ValueError(
        'Dimension Mismatch: Q must be square. Got dimensions ({}, {})'.format(
          p, p1
        )
      )

    if p != p2:
      raise ValueError(
        'Dimension Mismatch: Q and P must have the same number of '
        'rows. Got dimensions ({}, {}) and ({}, {})'.format(
            p, p1, p2, m
        )
      )

    self.Q = Q
    self.P = P
    self.p = p
    self.m = m

    self._W = None
    self._V = None

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  @property
  def shape(self):
    '''Determine the state of this DSF.

    Given the dimensions of Q as p x p and the dimensions of P as p x m, the
    shape is (p, m).
    '''
    return self.p, self.m

  #------------------------------------------------------------
  def is_wellposed(self):
    '''Determine whether this network is well-posed, according to the definition
    in [1,2].

    Returns
    -------
    iswp : bool
      True if this network is well-posed (i.e., (I - Q) has a proper
      inverse); False otherwise.

    Sources
    -------
    [1] N. Woodbury, A. Dankers and S. Warnick, "On the Well-Posedness of
        LTI Networks," Conference on Decision and Control, Melbourne,
        Australia, 2017.
    [2] N. Woodbury, A. Dankers and S. Warnick, "Dynamic Networks:
        Representations, Abstractions, and Well-Posedness," Conference on
        Decision and Control, Miami Beach, Florida, 2018.
    '''
    from . import DTTF

    ImQ = DTTF(Matrix.eye(self.p)) - self.Q
    ImQ_oo = ImQ.limit(oo)
    return rank(ImQ_oo) == self.p

  #------------------------------------------------------------
  def immerse(self, S):
    '''Find the immersion of this DSF where only outputs in S (zero indexed)
    are manifest.

    TODO : when DiscreteDNF is added, separate implementation into a node
    abstraction followed by an edge abstraction.

    Attributes
    ----------
    (Only access these immediately after the abstract function is called,
    they are read only and are designed to show intermediate computation.
    For `F = DTDSF()`, access the attributes with `F._W` or `F._V`)
    _W : DTTF
      The intermediate W computed. Note that this would be the W in the DNF
      if only a node abstraction and not an edge abstraction is used.
    _V : DTTF
      The intermediate V computed. Note that this would be the V in the DNF
      if only a node abstraction and not an edge abstraction is used.

    Parameters
    ----------
    S : list
      A list of indices that remain manifest in the immersion (zero indexed).

    Returns
    -------
    FS : DTDSF
      The immersed DSF.
    '''
    from . import DTTF

    U = set(range(self.p))
    S = set(S)
    S &= U
    Sbar = U - S

    S = list(S)
    S.sort()
    Sbar = list(Sbar)
    Sbar.sort()

    Q = self.Q
    P = self.P

    Q11 = Q[S, S]
    Q12 = Q[S, Sbar]
    Q21 = Q[Sbar, S]
    Q22 = Q[Sbar, Sbar]

    P11 = P[S, :]
    P22 = P[Sbar, :]

    I11 = DTTF(eye(Q11.shape[0]))
    I22 = DTTF(eye(Q22.shape[0]))
    inner = Q12 * (I22 - Q22).inv()
    W = Q11 + inner * Q21
    V = P11 + inner * P22

    self._W = W
    self._V = V

    DW = W.diag()

    ImDW = (I11 - DW).inv()
    Q = ImDW * (W - DW)
    P = ImDW * V

    return DTDSF(Q, P)

  #####################################################################
  #   Conversions
  #####################################################################

  #------------------------------------------------------------
  def to_TF(self):
    '''Convert this DSF into a transfer function.

    Returns
    -------
    G : DTTF
    '''
    from . import DTTF

    Iq = DTTF(eye(self.Q.shape[0]))
    G = (Iq - self.Q).inv() * self.P
    return G

  #------------------------------------------------------------
  def to_convolutional(self):
    '''Converts this DSF into a convolutional DSF.

    Returns
    -------
    ConvolutionalDSF
    '''
    from . import ConvolutionalDSF

    Qconv = self.Q.to_convolutional()
    Pconv = self.P.to_convolutional()
    return ConvolutionalDSF(Qconv, Pconv)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    # return repr(self.G)
    return 'Q = {}\nP = {}'.format(repr(self.Q), repr(self.P))

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Return the latex formatted version of this DSF.

    Parameters
    ----------
    Qname : str (latex formatting)
        The name to give to the Q matrix.
    Pname : str (latex formatting)
        The name to give to the P matrix.

    Returns
    -------
    latex : str
    '''
    Qname = kwargs.get('Qname', 'Q')
    Pname = kwargs.get('Pname', 'P')
    return r'{} \qquad {}'.format(
      self.Q.latex(name=Qname), self.P.latex(name=Pname)
    )
