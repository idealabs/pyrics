#------------------------------------------------------------
# pyrics/Representations/SystemRepresnetation.py
#
# Base representation for all models.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from IPython.display import display, Math


class SystemRepresentation():
  '''Common Functionality for all representations.
  '''

  #------------------------------------------------------------
  def display(self, **kwargs):
    '''Display the compiled latex representation within a
    jupyter/ipython/hydrogen environment.

    Parameters
    ----------
    **kwargs : key word arguments (optional)
      All arguments to pass in to the `latex()` method implemented by
      any child.
    '''
    return display(Math(self.latex(**kwargs)))

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''To be implemented in any child class. Key word arguments can and
    should be named and specified within any implementation.
    '''
    raise NotImplementedError()
