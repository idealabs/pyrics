import numpy as np
import sympy as sp
from sympy import latex
from pyrics.Representations import DTSS
from IPython.display import display

# In []:

A = np.array([
    [0.75, 0, 0, 0, 0, 1.2],
    [-.1, -.35, 0, 0, 0, 0],
    [0, 0, 0.85, -1, 0, 0],
    [0, -0.73, 0, 0.95, 0, 0],
    [0, 0, 0.43, 0, -0.6, 0],
    [0, 0, 0, 0, 0.2, 0.55]
])
B = np.array([
    [1.4, 0, -1.4],
    [0, -0.25, 0],
    [0, 0, 0.75],
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
])

C = np.array(
    [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0]
    ]
)

ss = DTSS(A, B, C)
F = ss.to_DSF()

F.display()

# In []:

ssfull = DTSS(A, B, np.eye(6))
Ffull = ssfull.to_DSF()

Ffull.display(Qname='Q_{full}', Pname='P_{full}')

# In []:

FS = Ffull.abstract([0, 1, 2])
FS.display(Qname='Q_S', Pname='P_S')
