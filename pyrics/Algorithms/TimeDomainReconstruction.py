#------------------------------------------------------------
# pyrics/Algorithms/TimeDomainReconstruction.py
#
# The time-domain network reconstruction algorithm.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from functools import partial
import numpy as np

from pyrics.utilities import vprint as vprintfull
from pyrics.Algorithms.Informativity import checkInformativity
from pyrics.Representations.ImpulseScalar import ImpulseScalar
from pyrics.Representations.ImpulseTF import ImpulseTF
from pyrics.Representations.ImpulseDSF import ImpulseDSF

#------------------------------------------------------------
def timeReconstruct(Du, Dy, r, K=None, tol=0.001, precision=1e-6,
                    verbose=False):
  '''Reconstruct an impulse response from input data Du and output data Dy.

  Parameters
  ----------
  Du : numpy array (T x m)
    The input data, where the t'th row of Du is u(t) (transpose).
  Du : numpy array (T x p)
    The output data, where the t'th row of Dy is y(t) (transpose).
  r : int s.t. 0 < r <= T
    Must be large enough such that every impulse response in the reconstructed
    Q(t) and P(t) is approximately 0 for every t > r.
  K : np.array (p^2 + pm x pm) or None, default=None
    The informativity conditions required to map the TF to the DSF uniquely. If
    None, assumes that P is diagonal (target specificity); however, an exception
    is raised in this case if p != m.
  tol : number or None
    If not None, performs a check on the goodness of the choice of r using
    the singular values of L. Let s1 be the rpm'th singular value and s2 be
    the (rpm + 1)'th singular value. Then, a warning is given if
    (s1 - s2) / s1 <= 1 - tol (since s2 should be small compared to s1).
  precision : number > 0
    Sets `ImpulseScalar.precision` for every impulse response generated.
  verbose : bool
    If True, prints status messages as the algorithm processes.

  Returns
  -------
  QP : ImpulseDSF
    The reconstructed network.
  '''
  vprint = partial(vprintfull, verbose=verbose)
  vprint('Initializing Algorithm')

  self = timeReconstruct
  T, m = Du.shape
  T1, p = Dy.shape

  assert p == m  # TODO - generalize

  if T1 != T:
    raise ValueError((
      'Du and Dy must be the same length (same number of rows). Given '
      'Du with {} rows and Dy with {} rows.'
    ).format(T, T1))

  vprint('Checking the Informativity Conditions')
  K, _ = checkInformativity(K, p, m)

  vprint('Building yvec')
  self.yvec = Dy.reshape((T * p, 1))  # numpy does row-major order by default

  vprint('Building M')
  self.M = _buildM(Du, Dy, r, K)

  vprint('Running Least Squares to fit the impulse responses')
  theta, _, _, s = np.linalg.lstsq(self.M, self.yvec, rcond=None)
  theta = theta.reshape((r * p * p, 1))

  # Check singular values:
  if tol is not None:
    vprint('Checking the singular values')
    # Otherwise the singular values are zero, which is good
    if len(s) >= r * p * m + 1:
      ratio = (s[r * p * m - 1] - s[r * p* m]) / s[r * p * m]
      if (1 - ratio) >= tol:
        raise Warning((
          'Singular value tolerance violated (ratio={:.5f}, should be close to '
          '1); likely r is too small.'
        ).format(ratio))

  vprint('Extracting Q(t) and P(t) from theta')
  Q, P = _extractQP(theta, p, m, r, K, precision)

  vprint('Collecting and returning')
  return ImpulseDSF(Q, P)


def _buildM(Du, Dy, r, K):
  '''Builds the M matrix required for reconstruction, reducing according to
  the informativity conditions.

  TODO : stop hard-coding informativity

  Parameters
  ----------
  Du : numpy array (T x m)
    The input data, where the t'th row of Du is u(t) (transpose).
  Du : numpy array (T x p)
    The output data, where the t'th row of Dy is y(t) (transpose).
  r : int s.t. 0 < r <= T
    Must be large enough such that every impulse response in the reconstructed
    Q(t) and P(t) is approximately 0 for every t > r.
  K : np.array (p^2 + pm x pm)
    The informativity conditions required to map the TF to the DSF uniquely.

  Returns
  -------
  L : np.array (pT x r(pm + p^2))
  '''
  T, m = Du.shape
  _, p = Dy.shape

  Mbar_cache = {}

  #-----------
  def Ybar(k):
    # Build Ybark
    Ybark = np.zeros((p, p * p))
    for i in range(p):
      y = Dy[k, :].reshape((1, p))
      Ybark[i, i * p : (i + 1) * p] = y

    return Ybark

  #-----------
  def Ubar(k):
    # Build Ubark
    Ubark = np.zeros((p, m * p))
    for i in range(p):
      u = Du[k, :].reshape((1, m))
      Ubark[i, i * m : (i + 1) * m] = u

    return Ubark

  #-----------
  def Mbar(k):
    # Check if cached
    if k in Mbar_cache:
      return Mbar_cache[k]

    # Build Mbark
    Ybark = Ybar(k)
    Ubark = Ubar(k)
    Mbark = np.concatenate((Ybark, Ubark), axis=1)

    # Utilize Informativity
    Mbark = Mbark.dot(K)

    # Cache and return
    Mbar_cache[k] = Mbark
    return Mbark

  # Build M
  _, width = K.shape
  M = np.zeros((p * T, r * width))
  for i in range(T):
    for j in range(r):
      k = i - j
      if k < 0:
        break

      M[i * p: (i + 1) * p, j * width: (j + 1) * width] = Mbar(k)

  return M

def _extractQP(theta, p, m, r, K, precision):
  '''Extract Q and P from theta.

  Parameters
  ----------
  theta : np.array (r * width x 1)
    The learned parameters in the impulse responses of Q and P.
  p : int > 0
  m : int > 0
  r : int > 0
  K : np.array (p^2 + pm x width)
  precision : number > 0

  Returns
  -------
  Q : ImpulseTF (p x p)
  P : ImpulseTF (p x m)
  '''
  Qa = []
  Pa = []
  for i in range(p):
    Qa.append([])
    Pa.append([])
    for j in range(p):
      Qa[i].append([])
    for j in range(m):
      Pa[i].append([])

  _, width = K.shape
  for k in range(r):
    x = theta[k * width : (k + 1) * width, :]
    vec = K.dot(x)

    ix = 0
    for i in range(p):
      for j in range(p):
        Qa[i][j].append(vec[ix, 0])
        ix += 1

    for i in range(p):
      for j in range(m):
        Pa[i][j].append(vec[ix, 0])
        ix += 1

  Q = []
  P = []
  for i in range(p):
    Q.append([])
    P.append([])
    for j in range(p):
      Q[i].append(ImpulseScalar(Qa[i][j], precision=precision))
    for j in range(m):
      P[i].append(ImpulseScalar(Pa[i][j], precision=precision))

  Q = ImpulseTF(Q)
  P = ImpulseTF(P)
  return Q, P
