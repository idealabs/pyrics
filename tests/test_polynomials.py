"""Tests the `is_polynomial` function from Utilities.
"""
import pytest
from sympy import sin, exp
from sympy.abc import x
from pyrics.PolynomialUtilities import (
    is_polynomial, is_rational_polynomial, is_proper, is_strictly_proper
)


def test_polynomials():
    assert is_polynomial(x, x)
    assert is_polynomial(1, x)
    assert is_polynomial(0, x)
    assert is_polynomial(2*x, x)
    assert is_polynomial(x ** 3, x)
    assert is_polynomial(2 * x ** 3, x)
    assert is_polynomial(5 * x ** 4 + 4 * x ** 3 + 3 * x ** 2 + 2 * x + 1, x)


def test_not_polynomials():
    assert not is_polynomial(x ** 0.5, x)
    assert not is_polynomial(sin(x), x)
    assert not is_polynomial(x ** 2 + 2 * sin(x), x)
    assert not is_polynomial(exp(-x), x)


def test_rational_polynomials():
    assert is_rational_polynomial(0, x)
    assert is_rational_polynomial(1, x)
    assert is_rational_polynomial(x, x)
    assert is_rational_polynomial(1 / x, x)
    assert is_rational_polynomial(x / (x - 1) ** 2, x)
    assert is_rational_polynomial(((x + 1) * (x + 2)) / ((x + 3) * (x + 4)), x)


def test_not_rational_polynomials():
    assert not is_rational_polynomial(sin(x), x)
    assert not is_rational_polynomial(1 / sin(x), x)
    assert not is_rational_polynomial(exp(x) / ((x + 1) * (x + 2)), x)


def test_is_proper():
    assert is_proper(0, x)
    assert is_proper(1, x)
    assert is_proper(1 / x, x)
    assert is_proper(x / (x + 1) ** 2, x)
    assert is_proper(((x + 1) * (x + 2)) / ((x + 3) * (x + 4)), x)


def test_not_is_proper():
    assert not is_proper(x, x)
    assert not is_proper(x ** 2 / (x + 1), x)
    assert not is_proper(((x + 1) * (x + 2)) / (x + 3), x)


def test_proper_check_error():
    with pytest.raises(ValueError):
        is_proper(sin(x), x)


def test_is_strictly_proper():
    assert is_strictly_proper(0, x)
    assert is_strictly_proper(1 / x, x)
    assert is_strictly_proper(x / (x + 1) ** 2, x)


def test_not_is_stricly_proper():
    assert not is_strictly_proper(x, x)
    assert not is_strictly_proper(x ** 2 / (x + 1), x)
    assert not is_strictly_proper(((x + 1) * (x + 2)) / (x + 3), x)
    assert not is_strictly_proper(((x + 1) * (x + 2)) / ((x + 3) * (x + 4)), x)
    assert not is_strictly_proper(1, x)


def test_strictly_proper_check_error():
    with pytest.raises(ValueError):
        is_strictly_proper(sin(x), x)
