#------------------------------------------------------------
# pyrics/Representations/ImpulseDSF.py
#
# Representation of a DSF in impulse response form.
#
# Copyright 2019 Nathan Woodbury
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------

from .ImpulseTF import ImpulseTF
from .SystemRepresentation import SystemRepresentation

class ImpulseDSF(SystemRepresentation):
  '''Representation of a DSF where each entry is an impulse response.

  Models

      Y(t) = Q(t) * Y(t) + P(t) * U(t),

  where * is the convolution operation and each Q_ij(t), P_ij(t) is a finite
  impulse response of length r.

  Parameters
  ----------
  Q : ImpulseTF (p x p)
  P : ImpulseTF (p x m)
  '''

  #------------------------------------------------------------
  def __init__(self, Q, P):
    self.Q = Q
    self.P = P

    if not isinstance(Q, ImpulseTF):
      raise ValueError('Q must be an ImpulseTF; got type={}'.format(type))
    if not isinstance(P, ImpulseTF):
      raise ValueError('P must be an ImpulseTF; got type={}'.format(type))

    p, p1 = Q.shape
    p2, m = P.shape
    r = Q.r
    r1 = P.r

    if p != p1:
      raise ValueError(
        'Dimension Mismatch: Q must be square. Got dimensions ({}, {})'.format(
          p, p1
        )
      )

    if p != p2:
      raise ValueError(
        'Dimension Mismatch: Q and P must have the same number of '
        'rows. Got dimensions ({}, {}) and ({}, {})'.format(
            p, p1, p2, m
        )
      )

    if r != r1:
      raise ValueError(
        'Length of impulse responses in Q and P must match. '
        'Got r={} in Q and r={} in P'.format(r, r1)
      )

    self.p = p
    self.m = m
    self.r = r

    # Reconstruction caches
    self.Ybar_cache = None
    self.Ubar_cache = None

  #####################################################################
  #   Public Methods
  #####################################################################

  #------------------------------------------------------------
  @property
  def shape(self):
    '''Determine the state of this DSF.

    Given the dimensions of Q as p x p and the dimensions of P as p x m, the
    shape is (p, m).
    '''
    return self.p, self.m

  #------------------------------------------------------------
  @staticmethod
  def reconstruct(Du, Dy, r, K=None, tol=0.001, precision=1e-6,
                  verbose=False):
    '''Reconstruct a ImpulseDSF from data.

    This is a wrapper around `passiveReconstruct()` from `pyrics.Algorithms`;
    please refer to the documentation of that function for all information
    about this method.
    '''
    from pyrics.Algorithms.TimeDomainReconstruction import timeReconstruct
    return timeReconstruct(
      Du, Dy, r, K=K, tol=tol, precision=precision, verbose=verbose,
    )

  #####################################################################
  #   Conversion functions
  #####################################################################

  def to_convolutional(self, order=3, bounds=10, verbose=False, tol=1e-3,
                       njobs=None):
    '''Convert this ImpulseTF into a ConvolutionalTF.

    Parameters
    ----------
    order : int > 0
      The number of poles (`w`) to add in the convolutional form, meaning the
      convolutional form will have `2 * order + 1` parameters.
    bounds : int > 0
      All parameters in the convolutional form will be bounded to be between
      -bounds and bounds.
    verbose : bool, default=False
      If true, prints status messages.
    tol : number > 0
      If the one-norm of any impulse response is less than tol, then 0 will
      be returned instead of attempting to convert to a convolutional scalar.
    njobs : int > 0 or None, default=None
      If not None, paralellizes the conversion of each scalar across a different
      job. Note that will run conversion of Q and P in sequence.

    Returns
    -------
    QP : ConvolutionalDSF
    '''
    from .ConvolutionalDSF import ConvolutionalDSF

    Q = self.Q.to_convolutional(
      order=order, bounds=bounds, verbose=verbose, tol=tol, njobs=njobs,
      name='Q'
    )
    P = self.P.to_convolutional(
      order=order, bounds=bounds, verbose=verbose, tol=tol, njobs=njobs,
      name='P'
    )

    return ConvolutionalDSF(Q, P)

  #####################################################################
  #   Printing and representation functions
  #####################################################################

  #------------------------------------------------------------
  def __repr__(self):
    return 'Q = {}\nP = {}'.format(repr(self.Q), repr(self.P))

  #------------------------------------------------------------
  def latex(self, **kwargs):
    '''Returns the latex formatted version of this transfer function.

    kwargs
    ----------
    name : str or None, default = G
      Prefixes the latex with 'name = ' for whatever name is given. If
      name is None, just gives the equation.
    length : int > 0, default=3
      Truncates the impulse response representation to the first `length` items.

    Returns
    -------
    latex : str
    '''
    Qname = kwargs.get('Qname', 'Q(t)')
    Pname = kwargs.get('Pname', 'P(t)')

    return r'{} \qquad {}'.format(
      self.Q.latex(name=Qname, **kwargs), self.P.latex(name=Pname, **kwargs)
    )

  #------------------------------------------------------------
  def get_plotly(self, Qname='Q(t)', Pname='P(t)', Qplot=None, Pplot=None,
                 Qtitle='', Ptitle='', scale=200, **kwargs):
    '''Generate and return plotly data for a time-series representation of
    this matrix of impulse responses. If `plot`, also draws a plotly figure.

    Parameters
    ----------
    Qname : str, default='Q(t)'
      The prefix to assign to each time series and plot title for the plot of Q.
    Pname : str, default='Q(t)'
      The prefix to assign to each time series and plot title for the plot of P.
    Qplot : function (plotly plot function) or None, default=None
      If None, only generates and returns the data. If a function is given,
      plots a single plot of the impulse responses in Q using that function.
    Pplot : function (plotly plot function) or None, default=None
      If None, only generates and returns the data. If a function is given,
      plots a single plot of the impulse responses in P using that function.
    Qtitle : str, default=''
      Only used if Qplot is not None. The title to assign to Q's plot.
    ptitle : str, default=''
      Only used if Pplot is not None. The title to assign to P's plot.
    scale : int > 0
      The dimensions of the figures will be p * scale x p * scale * 1.5

    kwargs
    ------
    Any additional parameter that can be used by `go.Scatter`. These will all be
    passed in to every scalar impulse responses plot function.

    Returns
    -------
    Qtraces : dict (tuple -> plotly.graph_objs)
      Maps (i, j) for the impulse response Q_ij(t) to the plot trace.
    Ptraces : dict (tuple -> plotly.graph_objs)
      Maps (i, j) for the impulse response P_ij(t) to the plot trace.
    '''
    Qtraces = self.Q.get_plotly(
      name=Qname, plot=Qplot, title=Qtitle, scale=scale, **kwargs
    )
    Ptraces = self.P.get_plotly(
      name=Pname, plot=Pplot, title=Ptitle, scale=scale, **kwargs
    )

    return Qtraces, Ptraces
